<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%--<link rel="stylesheet" type="text/css" href="../css/loginPage.css"> --%>

<style type="text/css">
@import url('css/loginPage.css');
</style>
<script type="text/javascript">
	window.history.forward();
	function noBack() {
		window.history.forward();
	}
</script>
<link rel="shortcut icon" href="images/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>S.M.P:Create Advertisement</title>
</head>
<body onload="noBack();" onpageshow="if (event.persisted) noBack();"
	onunload="">

	<div class="header">
		<span class="titleHeader"><font color="blue">S</font><font
			color="red">.M</font>.<font color="#E3A848">P</font> </span> <a
			href="loginPage.jsp"><input type="button" value="Log out"
			class="buttonLogOut" /> </a>
	</div>
	<div class="signupError adsError font"><h2>${createAdMessage}</h2></div>
	
	<form action="CreateAdsController" method="post">
		<div class="phoneTextSpan font">
			<span class="loginNameFont">Logged in as, ${lastName} ${firstName} </span> <br />
			<br /> Follow the below steps for a successful transaction<br /> <br>
			<center>
				1.Specify correct address<br /> <br /> 2.Specify the exact nature
				of the product<br /> <br /> 3.Give a good deal to the user<br />
				<br />
				<h3>Receive your amount in 30 minutes!!</h3>
			</center>
		</div>
		<div class="phoneSpan">
			<img src="images/phone.png" alt="phone">
		</div>
		<div class="mainDiv">
			<span class="labelSignIn fontTextFields"><strong>Create
					an advertisement</strong> </span> <span class="labelUserName fontTextFields"><strong>Item
					Id</strong> </span> <input type="text" class="textBoxSpan font"
				autofocus="autofocus" name="itemId"> <span
				class="labelUserName fontTextFields"><strong>Item
					Name</strong> </span> <input type="text" class="textBoxSpan font" name="itemName">
			<span class="labelUserName fontTextFields"><strong>Item
					Description</strong> </span> <input type="text" class="textBoxSpan font"
				name="itemDescription"> <span
				class="labelUserName fontTextFields"><strong>Item
					Price (in $)</strong> </span> <input type="text" class="textBoxSpan font"
				name="itemPrice"> <span class="labelUserName fontTextFields"><strong>Quantity
					Available (in pieces)</strong> </span> <input type="text" class="textBoxSpan font"
				name="quantity"> <span class="labelUserName fontTextFields"><strong>Seller
					Information</strong> </span> <input type="text" class="textBoxSpan font"
				name="sellerInformation">
			<div class="signInSpan">
				<input type="submit" value="Create Ad"
					class="buttonSignIn font createAdButton" />
			</div>
		</div>
		<input type="hidden" value="${username}" name="username" />
	</form>
	  <a href="homePage.jsp"><span style="float:left;margin-top:-250px;margin-left:25px"><img src="images/home_button.png" width="100" title="Go to home!" height="100"/></span></a>
</body>
</html>