<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<%--<link rel="stylesheet" type="text/css" href="../css/loginPage.css"> --%>

<style type="text/css">
@import url('css/loginPage.css');
</style>


<link rel="shortcut icon" href="images/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>S.M.P:Sign Up</title>
</head>
<body>
	<div class="header">
		<span class="titleHeader"><font color="blue">S</font><font
			color="red">.M</font>.<font color="#E3A848">P</font> </span> <span
			class="signInHeader headerSignSpan">Already have an account in
			S.M.P ?</span><a href="loginPage.jsp"><input type="button" value="SIGN IN"
			class="buttonSignUp" /> </a>
	</div>
		<div class="signupError font"><h2>${signupMessage}</h2></div>
		
	<form action="SignupController" method="post">
		<div class="phoneTextSpan font">
			Welcome to the SimpleMarketPlace. Follow these simple 4 steps !<br />
			<br>
			<center>
				1.Login with your account<br /> <br /> 2.Select the item<br /> <br />
				3.Checkout<br /> <br /> 4.Pay<br /> <br />
				<h3>Now, you can Receive your item in an hour!!</h3>
			</center>
		</div>
		<div class="phoneSpan">
			<img src="images/phone.png" alt="phone">
		</div>
		<div class="mainDiv">
			<span class="labelSignIn fontTextFields"><strong>Sign up</strong></span><span
				class="labelUserName fontTextFields"><strong>First Name</strong></span><input type="text"
				class="textBoxSpan font" name="firstName" autofocus="autofocus">
				<span class="labelUserName fontTextFields"><strong>Last Name</strong></span><input
				type="text" class="textBoxSpan font" name="lastName"> <span
				class="labelUserName fontTextFields"><strong>Email</strong></span><input type="text"
				class="textBoxSpan font" name="emailid"> <span
				class="labelUserName fontTextFields"><strong>Password</strong></span><input type="password"
				class="textBoxSpan font" name="password">
			<div class="signInSpan">
				<input type="submit" value="Sign up" class="buttonSignIn fontTextFields" />
			</div>
		</div>
	</form>
</body>
</html>