package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class myProfile_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<!DOCTYPE HTML>\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("\t<title>Rakesh Buddhiraju</title>\r\n");
      out.write("\t<link type=\"text/css\" rel=\"stylesheet\" href=\"v3/css/bootstrap.css\"/>\r\n");
      out.write("\t<link href=\"http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\" rel=\"stylesheet\">\r\n");
      out.write("\t<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js\"></script>\r\n");
      out.write("\t<script src=\"v3/js/bootstrap.js\"></script>\r\n");
      out.write("\t\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("\t<div class=\"navbar navbar-default navbar-fixed-top\">\r\n");
      out.write("\t\t<div class=\"container\">\r\n");
      out.write("\t\t\t<div class=\"navbar-header\">\r\n");
      out.write("\t\t\t\t<button class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">\r\n");
      out.write("\t\t\t\t<span class=\"icon-bar\">\r\n");
      out.write("\t\t\t\t</span><span class=\"icon-bar\">\r\n");
      out.write("\t\t\t\t</span><span class=\"icon-bar\">\r\n");
      out.write("\t\t\t\t</span>\r\n");
      out.write("\t\t\t\t</button>\r\n");
      out.write("\t\t\t\t<a href=\"#\" class=\"navbar-brand\">Rakesh Buddhiraju Tutorial</a>\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t\t<div class=\"navbar-collapse collapse\">\r\n");
      out.write("                <ul class=\"nav navbar-nav\">\r\n");
      out.write("                    <li><a href=\"index.html\"><span class=\"glyphicon glyphicon-home\"></span> Home</a></li>\r\n");
      out.write("                    <li><a href=\"about.html\"><span class=\"glyphicon glyphicon-user\"></span> About</a></li>\r\n");
      out.write("                    <li><a href=\"feature.html\"><span class=\"glyphicon glyphicon-picture\"></span> Features</a></li>\r\n");
      out.write("                    <li class=\"dropdown\">\r\n");
      out.write("                        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><span class=\"glyphicon glyphicon-lock\"></span> Tutorials<b class=\"caret\"></b></a>\r\n");
      out.write("                        <ul class=\"dropdown-menu\">\r\n");
      out.write("                            <li><a href=\"#\">Bootstrap Tutorials</a></li>\r\n");
      out.write("                            <li><a href=\"#\">PHP Tutorials</a></li>\r\n");
      out.write("                            <li><a href=\"#\">Java Tutorials</a></li>\r\n");
      out.write("                            <li><a href=\"#\">CSS Tutorials</a></li>\r\n");
      out.write("                        </ul>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li><a href=\"contact.html\"><span class=\"glyphicon glyphicon-phone\"></span> Contact</a></li>\r\n");
      out.write("                </ul>\r\n");
      out.write("\t\t\t\t<form class=\"navbar-form navbar-right\" role=\"search\">\r\n");
      out.write("              <div class=\"form-group\">\r\n");
      out.write("                <input type=\"text\" class=\"form-control\" placeholder=\"Search\">\r\n");
      out.write("              </div>\r\n");
      out.write("              <button type=\"submit\" class=\"btn btn-default\">Submit</button>\r\n");
      out.write("            </form>\r\n");
      out.write("            </div>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t\r\n");
      out.write("\t\t  <!-- Carousel\r\n");
      out.write("    =========================-->\r\n");
      out.write("    \r\n");
      out.write("    <div id=\"myCarousel\" class=\"carousel slide\">\r\n");
      out.write("        \r\n");
      out.write("        <ol class=\"carousel-indicators\">\r\n");
      out.write("            <li data-target=\"#myCarousel\" data-slide-to=\"0\" class=\"active\"></li>\r\n");
      out.write("            <li data-target=\"#myCarousel\" data-slide-to=\"1\"></li>\r\n");
      out.write("            <li data-target=\"#myCarousel\" data-slide-to=\"2\"></li>\r\n");
      out.write("        </ol>\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t<div class=\"carousel-inner\">\r\n");
      out.write("            \r\n");
      out.write("            <div class=\"item active\">\r\n");
      out.write("               <img src=\"v3/img1 (4).jpg\">\r\n");
      out.write("                <div class=\"container\">\r\n");
      out.write("                    <div class=\"carousel-caption\">\r\n");
      out.write("                        <h1>My Heading Text</h1>\r\n");
      out.write("                        <p>This tag will contain the text which we want to appear on our slide.</p>\r\n");
      out.write("                        <p><a class=\"btn btn-large btn-primary\">Sign Up</a></p>\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("\t\t\t<div class=\"item\">\r\n");
      out.write("               <img src=\"v3/img1 (5).jpg\">\r\n");
      out.write("                <div class=\"container\">\r\n");
      out.write("                    <div class=\"carousel-caption\">\r\n");
      out.write("                        <h1>My Heading Text</h1>\r\n");
      out.write("                        <p>This tag will contain the text which we want to appear on our slide.</p>\r\n");
      out.write("                        <p><a class=\"btn btn-large btn-primary\">Sign Up</a></p>\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("\t\t\t<div class=\"item\">\r\n");
      out.write("               <img src=\"v3/img1 (6).jpg\">\r\n");
      out.write("                <div class=\"container\">\r\n");
      out.write("                    <div class=\"carousel-caption\">\r\n");
      out.write("                        <h1>My Heading Text</h1>\r\n");
      out.write("                        <p>This tag will contain the text which we want to appear on our slide.</p>\r\n");
      out.write("                        <p><a class=\"btn btn-large btn-primary\">Sign Up</a></p>\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t\t<a class=\"left carousel-control\" href=\"#myCarousel\" data-slide=\"prev\">\r\n");
      out.write("            <span class=\"glyphicon glyphicon-chevron-left\"></span>\r\n");
      out.write("        </a>\r\n");
      out.write("        \r\n");
      out.write("        <a class=\"right carousel-control\" href=\"#myCarousel\" data-slide=\"next\">\r\n");
      out.write("            <span class=\"glyphicon glyphicon-chevron-right\"></span>\r\n");
      out.write("        </a>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t\r\n");
      out.write("    <!-- Grid\r\n");
      out.write("    ==========================-->\r\n");
      out.write("\t <div class=\"container\">\r\n");
      out.write("        <div class=\"row\">\r\n");
      out.write("            \r\n");
      out.write("            <div class=\"col-md-4\">\r\n");
      out.write("                <h2>First Heading</h2>\r\n");
      out.write("                <p>Some text will go here. Some text will go here. Some text will go here. Some text will go here. </p>\r\n");
      out.write("            </div>\r\n");
      out.write("            \r\n");
      out.write("            <div class=\"col-md-4\">\r\n");
      out.write("                <h2>First Heading</h2>\r\n");
      out.write("                <p>Some text will go here. Some text will go here. Some text will go here. Some text will go here. </p>\r\n");
      out.write("            </div>\r\n");
      out.write("             <div class=\"col-md-4\">\r\n");
      out.write("                <h2>First Heading</h2>\r\n");
      out.write("                <p>Some text will go here. Some text will go here. Some text will go here. Some text will go here. </p>\r\n");
      out.write("            </div>\r\n");
      out.write("           \r\n");
      out.write("        </div>\r\n");
      out.write("        \r\n");
      out.write("        <div class=\"row\">\r\n");
      out.write("            <div class=\"col-md-6\">\r\n");
      out.write("                <h1>First Heading</h1>\r\n");
      out.write("                <p>\r\n");
      out.write("                    Some text will go here. Some text will go here. Some text will go here.\r\n");
      out.write("                    Some text will go here. Some text will go here. Some text will go here. Some text will go here. \r\n");
      out.write("                </p>\r\n");
      out.write("            </div>\r\n");
      out.write("            \r\n");
      out.write("            <div class=\"col-md-6\">\r\n");
      out.write("                <h1>First Heading</h1>\r\n");
      out.write("                <p>\r\n");
      out.write("                    Some text will go here. Some text will go here. Some text will go here.\r\n");
      out.write("                    Some text will go here. Some text will go here. Some text will go here. Some text will go here. \r\n");
      out.write("                </p>\r\n");
      out.write("            </div>\r\n");
      out.write("            \r\n");
      out.write("        </div>\r\n");
      out.write("        \r\n");
      out.write("    </div>\r\n");
      out.write("\t<hr/><hr/>\t\r\n");
      out.write("\t  <!-- Font Awesome Icons\r\n");
      out.write("    ============================-->\r\n");
      out.write("    \r\n");
      out.write("    <div class=\"container\">\r\n");
      out.write("        <div class=\"row\">\r\n");
      out.write("            <div class=\"col-md-12\">\r\n");
      out.write("                   <div class=\"well\">\r\n");
      out.write("                    <a href=\"http://www.meetup.com/AndroidMakers/member/118575242/\" title=\"Visit Meetup Profile\"> <i class=\"longicon fa fa-android fa-fw\"></i> </a>\r\n");
      out.write("                    <a href=\"http://www.linkedin.com/in/venkatamaninagarakesh\" title=\"Visit LinkedIn Profile\"> <i class=\"longicon fa fa-linkedin fa-fw\"></i></a>\r\n");
      out.write("                    <a href=\"https://twitter.com/nagarakesh4\" title=\"Visit twitter Profile\"> <i class=\"longicon fa fa-twitter fa-fw\"></i></a>\r\n");
      out.write("                    <a href=\"http://www.youtube.com/watch?v=qc-0FIXqLXQ\" title=\"Visit Youtube Profile\"> <i class=\"longicon fa fa-youtube fa-5x\"></i></a>\r\n");
      out.write("                    <a href=\"http://www.skype.com/en/\" title=\"Visit Skype Profile\"> <i class=\"longicon fa fa-skype fa-5x\"></i></a>\r\n");
      out.write("                    <a href=\"https://plus.google.com/117383073401130612503\" title=\"Visit Google+ Profile\"> <i class=\"longicon fa fa-google-plus fa-5x\"></i></a>\r\n");
      out.write("                    <a href=\"http://nagarakesh.blogspot.com/\" title=\"Visit Blog Profile\"> <i class=\"longicon fa fa-apple fa-5x\"></i></a>\r\n");
      out.write("\t\t\t\t\t</div>\r\n");
      out.write("            </div>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t \r\n");
      out.write("    <div class=\"container\">\r\n");
      out.write("        <div class=\"row\">\r\n");
      out.write("            <div class=\"col-md-12\">\r\n");
      out.write("                \r\n");
      out.write("                <hr/>\r\n");
      out.write("                <p>Copyright &copy; Creativity Tuts.\r\n");
      out.write("                    <a data-toggle=\"modal\" href=\"#myModal\">Terms and Conditions</a>\r\n");
      out.write("                </p>\r\n");
      out.write("                \r\n");
      out.write("                <!-- Modal -->\r\n");
      out.write("                <div class=\"modal fade\" id=\"myModal\" tabinex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\r\n");
      out.write("                    <div class=\"modal-dialog\">\r\n");
      out.write("                        <div class=\"modal-content\">\r\n");
      out.write("                            <div class=\"modal-header\">\r\n");
      out.write("                                <h2>Terms and Conditions</h2>\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <div class=\"modal-body\">\r\n");
      out.write("                                <p>The text will go here.....</p>\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <div class=\"modal-footer\">\r\n");
      out.write("                                <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Close</button>\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("                \r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("    </div>\r\n");
      out.write("\t</div>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
