package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class createAdvertisement_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<style type=\"text/css\">\r\n");
      out.write("@import url('css/loginPage.css');\r\n");
      out.write("</style>\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("\twindow.history.forward();\r\n");
      out.write("\tfunction noBack() {\r\n");
      out.write("\t\twindow.history.forward();\r\n");
      out.write("\t}\r\n");
      out.write("</script>\r\n");
      out.write("<link rel=\"shortcut icon\" href=\"images/favicon.ico\" />\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\r\n");
      out.write("<title>S.M.P:Create Advertisement</title>\r\n");
      out.write("</head>\r\n");
      out.write("<body onload=\"noBack();\" onpageshow=\"if (event.persisted) noBack();\"\r\n");
      out.write("\tonunload=\"\">\r\n");
      out.write("\r\n");
      out.write("\t<div class=\"header\">\r\n");
      out.write("\t\t<span class=\"titleHeader\"><font color=\"blue\">S</font><font\r\n");
      out.write("\t\t\tcolor=\"red\">.M</font>.<font color=\"#E3A848\">P</font> </span> <a\r\n");
      out.write("\t\t\thref=\"loginPage.jsp\"><input type=\"button\" value=\"Log out\"\r\n");
      out.write("\t\t\tclass=\"buttonLogOut\" /> </a>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t<div class=\"signupError adsError font\"><h2>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${createAdMessage}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("</h2></div>\r\n");
      out.write("\t\r\n");
      out.write("\t<form action=\"CreateAdsController\" method=\"post\">\r\n");
      out.write("\t\t<div class=\"phoneTextSpan font\">\r\n");
      out.write("\t\t\t<span class=\"loginNameFont\">Logged in as, ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${lastName}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write(' ');
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${firstName}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write(" </span> <br />\r\n");
      out.write("\t\t\t<br /> Follow the below steps for a successful transaction<br /> <br>\r\n");
      out.write("\t\t\t<center>\r\n");
      out.write("\t\t\t\t1.Specify correct address<br /> <br /> 2.Specify the exact nature\r\n");
      out.write("\t\t\t\tof the product<br /> <br /> 3.Give a good deal to the user<br />\r\n");
      out.write("\t\t\t\t<br />\r\n");
      out.write("\t\t\t\t<h3>Receive your amount in 30 minutes!!</h3>\r\n");
      out.write("\t\t\t</center>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t<div class=\"phoneSpan\">\r\n");
      out.write("\t\t\t<img src=\"images/phone.png\" alt=\"phone\">\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t<div class=\"mainDiv\">\r\n");
      out.write("\t\t\t<span class=\"labelSignIn fontTextFields\"><strong>Create\r\n");
      out.write("\t\t\t\t\tan advertisement</strong> </span> <span class=\"labelUserName fontTextFields\"><strong>Item\r\n");
      out.write("\t\t\t\t\tId</strong> </span> <input type=\"text\" class=\"textBoxSpan font\"\r\n");
      out.write("\t\t\t\tautofocus=\"autofocus\" name=\"itemId\"> <span\r\n");
      out.write("\t\t\t\tclass=\"labelUserName fontTextFields\"><strong>Item\r\n");
      out.write("\t\t\t\t\tName</strong> </span> <input type=\"text\" class=\"textBoxSpan font\" name=\"itemName\">\r\n");
      out.write("\t\t\t<span class=\"labelUserName fontTextFields\"><strong>Item\r\n");
      out.write("\t\t\t\t\tDescription</strong> </span> <input type=\"text\" class=\"textBoxSpan font\"\r\n");
      out.write("\t\t\t\tname=\"itemDescription\"> <span\r\n");
      out.write("\t\t\t\tclass=\"labelUserName fontTextFields\"><strong>Item\r\n");
      out.write("\t\t\t\t\tPrice (in $)</strong> </span> <input type=\"text\" class=\"textBoxSpan font\"\r\n");
      out.write("\t\t\t\tname=\"itemPrice\"> <span class=\"labelUserName fontTextFields\"><strong>Quantity\r\n");
      out.write("\t\t\t\t\tAvailable (in pieces)</strong> </span> <input type=\"text\" class=\"textBoxSpan font\"\r\n");
      out.write("\t\t\t\tname=\"quantity\"> <span class=\"labelUserName fontTextFields\"><strong>Seller\r\n");
      out.write("\t\t\t\t\tInformation</strong> </span> <input type=\"text\" class=\"textBoxSpan font\"\r\n");
      out.write("\t\t\t\tname=\"sellerInformation\">\r\n");
      out.write("\t\t\t<div class=\"signInSpan\">\r\n");
      out.write("\t\t\t\t<input type=\"submit\" value=\"Create Ad\"\r\n");
      out.write("\t\t\t\t\tclass=\"buttonSignIn font createAdButton\" />\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t<input type=\"hidden\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${username}", java.lang.String.class, (PageContext)_jspx_page_context, null, false));
      out.write("\" name=\"username\" />\r\n");
      out.write("\t</form>\r\n");
      out.write("\t  <a href=\"homePage.jsp\"><span style=\"float:left;margin-top:-250px;margin-left:25px\"><img src=\"images/home_button.png\" width=\"100\" title=\"Go to home!\" height=\"100\"/></span></a>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
