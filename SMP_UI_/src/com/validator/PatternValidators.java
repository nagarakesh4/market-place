package com.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternValidators {



	public static boolean validateEmail(String txt){
		String regx = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(txt);
		return matcher.matches();
	}

	public static boolean validateUsername(String txt){

		String regx= "^[a-z0-9_-]{3,15}$";
		Pattern pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(txt);
		return matcher.matches();
	}
	public static boolean validateAlphabetsSensitivity(String txt) {

		String regx = "^[a-zA-Z]+(([\\'\\,\\.\\-][a-zA-Z])?[a-zA-Z]*)*$";
		Pattern pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(txt);
		return matcher.find();

	}
	public static boolean validatePassword(String txt){
		String regx = "^[a-zA-Z]\\w{3,14}$";
		Pattern pattern = Pattern.compile(regx);
		Matcher matcher = pattern.matcher(txt);
		return matcher.matches();
	}

	public static boolean validateLetters(String txt) {
		String regx = "^[a-zA-Z]+(([\\'\\,\\.\\-][a-zA-Z])?[a-zA-Z]*)*$";
		Pattern pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(txt);
		return matcher.find();
	}

}