package com.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.jws.WebService;

@WebService
public class PaymentService {
	public int checkOut(String buyerUserName,String ccnumber) {
		Connection con = null;
		int paymentStatus = 0;
		
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			con = DriverManager.getConnection("jdbc:mysql://localhost/test",
					"root", "root");
			// if the connection is successful
			if (!con.isClosed()) {
				System.out
						.println("Sucessfully Connected to MySql Server using TCP/IP");
				if(ccnumber.length()==16){
					paymentStatus = 1;
					
					// mysql to retrieve itemid and quantities present in the cart for this user
					String retrieveItemId = "select itemid,quantity,sellerUserName from cartadd where buyerUsername=?";
					
					//mysql to check if the item for this user is already present in his brought and sold table 
					String checkIfAlreadyPresent = "select * from checkedoutitems where itemid=? and buyerUserName=?";
					
					//mysql to update the quantities if the the quantity is already present 
					String updateQuantity = "update checkedoutitems set quantity=quantity+? where itemid=? and buyerUserName=?";
					
					// mysql to insert records in broughtsold table
					String storeFinalResults = "insert into test.checkedoutitems ( itemid, itemprice, buyerUserName,sellerUserName,quantity,itemname) select itemid, itemprice, buyerUserName, sellerUserName, quantity,itemname from test.cartadd a where a.buyerUserName=? and a.itemid=?";
					
					//mysql to update quantities for this item on the seller side (in ads table)
					String updateQuantitiesOfSeller = "update adsdetails set quantity=quantity-? where itemid=? and username=?";
					
					//mysql to remove cart items once checked out succeesfully for this user
					String deleteUsersCart = "delete from cartadd where buyerUserName=? ";
					
					PreparedStatement preparedStatementQ1 = con.prepareStatement(retrieveItemId);
					preparedStatementQ1.setString(1, buyerUserName);
					ResultSet resultSetRetrieveItemId = preparedStatementQ1.executeQuery();
					int totalItemIdInCart = 0,totalItemIdAndUserNameMatching = 0,updateQuantityStatus=0,insertStatus=0,deleteStatus=0;
					while (resultSetRetrieveItemId.next()) {
						System.out.println("this item "+ resultSetRetrieveItemId.getString(1)+ " is going to be checked now");
						PreparedStatement preparedStatementQ2 = con.prepareStatement(checkIfAlreadyPresent);
						preparedStatementQ2.setString(1, resultSetRetrieveItemId.getString(1));
						preparedStatementQ2.setString(2, buyerUserName);
						ResultSet resultSetCheckIfAlreadyPresent = preparedStatementQ2.executeQuery();
					
						System.out.println("deducting the quantities ordered by buyer from seller");
						PreparedStatement preparedStatementQ5 = con.prepareStatement(updateQuantitiesOfSeller);
						preparedStatementQ5.setInt(1, resultSetRetrieveItemId.getInt(2));
						preparedStatementQ5.setString(2, resultSetRetrieveItemId.getString(1));
						preparedStatementQ5.setString(3, resultSetRetrieveItemId.getString(3));
						
						updateQuantityStatus = preparedStatementQ5.executeUpdate();
						System.out.println("the status of seller quantity updation "+updateQuantityStatus);
					
						if(resultSetCheckIfAlreadyPresent.next()){
							System.out.println("this item "+resultSetCheckIfAlreadyPresent.getString(1)+" is present so update the quantity");
							System.out.println("just cross verifying " + resultSetRetrieveItemId.getString(1));
							PreparedStatement preparedStatementQ3 = con.prepareStatement(updateQuantity);
							preparedStatementQ3.setInt(1, resultSetRetrieveItemId.getInt(2));
							preparedStatementQ3.setString(2, resultSetRetrieveItemId.getString(1));
							preparedStatementQ3.setString(3, buyerUserName);
							updateQuantityStatus = preparedStatementQ3.executeUpdate();
							System.out.println("update status is "+updateQuantityStatus);
							totalItemIdAndUserNameMatching++;
						}else{
							System.out.println("the item "+resultSetRetrieveItemId.getString(1)+" is not in coutitems so insert");
							PreparedStatement preparedStatementQ4 = con.prepareStatement(storeFinalResults);
							preparedStatementQ4.setString(1, buyerUserName);
							preparedStatementQ4.setString(2, resultSetRetrieveItemId.getString(1));
							insertStatus = preparedStatementQ4.executeUpdate();
							System.out.println("insert status is" +insertStatus);
							totalItemIdAndUserNameMatching++;
						}
						totalItemIdInCart++;
						}
						
						System.out.println("total items in cart for this user "+(totalItemIdInCart-1));
						System.out.println("total items in cart matching with checkedout and not matching items "+(totalItemIdAndUserNameMatching));
						if((totalItemIdInCart)==(totalItemIdAndUserNameMatching)){
							
							System.out.println("successfully payment done now deleting from cart");
							PreparedStatement preparedStatementQ6 = con.prepareStatement(deleteUsersCart);
							preparedStatementQ6.setString(1, buyerUserName);
							deleteStatus = preparedStatementQ6.executeUpdate();
							System.out.println("delete status is " +deleteStatus);
						}
					}else{
						paymentStatus = 2;
				}
			}
		}
		catch(Exception exception){
			System.out.println(exception);
			exception.printStackTrace();
		}
		return paymentStatus;
	}
	
}