package com.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.jws.WebService;

import com.beans.AdsBean;

@WebService
public class ViewCartService {
	public AdsBean[] retrieveCart(String username) {
		Connection con = null;
		AdsBean[] adsBeanValues = new AdsBean[500];

		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			con = DriverManager.getConnection("jdbc:mysql://localhost/test",
					"root", "root");
			int index = 0;

			// if the connection is successful
			if (!con.isClosed()) {
				System.out
						.println("Sucessfully Connected to MySql Server using TCP/IP");

				// mysql to retrieve all the cart items present
				String sqlCheck = "select * from test.cartadd where buyerUserName=?";

				// mysql to calculate total amount of all the items in cart
				String totalPrice = "select sum(itemprice),sum(quantity) from test.cartadd where buyerUserName=?";
				
				PreparedStatement preparedStatement = con
						.prepareStatement(sqlCheck);
				preparedStatement.setString(1, username);
				ResultSet resultSet = preparedStatement.executeQuery();
				while (resultSet.next()) {
					System.out.println("processing to display all cart items");

					AdsBean adsBean = new AdsBean();
					adsBean.setItemId(resultSet.getString(1));
					adsBean.setItemName(resultSet.getString(2));
					adsBean.setItemDescription(resultSet.getString(3));
					adsBean.setItemPrice(resultSet.getFloat(4));
					adsBean.setQuantity(resultSet.getInt(5));
					adsBean.setSellerInformation(resultSet.getString(6));
					adsBean.setUserName(resultSet.getString(7));
					adsBean.setSellerUserName(resultSet.getString(8));
					adsBeanValues[index] = adsBean;
					index++;
				}
				
				preparedStatement = con
						.prepareStatement(totalPrice);
				preparedStatement.setString(1, username);
				resultSet = preparedStatement.executeQuery();
				if(resultSet.next()){
					AdsBean adsBean = new AdsBean();
					adsBean.setTotalValueOfItems(resultSet.getFloat(1));
					adsBean.setTotalItemsInCart(resultSet.getInt(2));
					System.out.println("total items in cart are "+adsBean.getTotalItemsInCart());
					adsBeanValues[index] = adsBean;
				}
			}
		} catch (Exception exception) {
			System.out.println("Problem in connecting");
			exception.printStackTrace();
			// TODO: handle exception
		}
		return adsBeanValues;
	}
}
