package com.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.jws.WebService;

import com.beans.AdsBean;

@WebService
public class RemoveFromCartService {
	public int[] removeFromCart(AdsBean adsBean) {
		Connection con = null;
		int removedStatus = 0;
		int[] resultsFromCart = new int[3];
		resultsFromCart[0] = 1;
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			con = DriverManager.getConnection("jdbc:mysql://localhost/test",
					"root", "root");
			if (!con.isClosed()) {
				System.out
						.println("Sucessfully Connected to MySql Server using TCP/IP");

				// mysql command to check if the number of quantities ordered
				// for this item
				String getNumberOfItems = "Select quantity from test.cartadd where buyerUsername=? and itemid=?";

				// update number of quantities ordered if necessary
				String updateQuantities = "update test.cartadd set quantity=quantity-1 where buyerUsername=? and itemid=?";

				// delete the row containing the required itemid
				String removeItem = "delete from test.cartadd where buyerUsername=? and itemid=?";

				// preparing for 'getNumberOfItems'
				PreparedStatement preparedStatement = con
						.prepareStatement(getNumberOfItems);
				preparedStatement.setString(1, adsBean.getUserName());
				preparedStatement.setString(2, adsBean.getItemId());
				ResultSet resultSet = preparedStatement.executeQuery();

				if (resultSet.next()) {
					// if there are more than 1 of same products then update the
					// cart
					if ((resultSet.getInt(1) > 1)) {
						System.out
								.println("this has more than 1 quantity ordered");
						System.out
								.println("so removing one and updating the cart");

						preparedStatement = con
								.prepareStatement(updateQuantities);
						preparedStatement.setString(1, adsBean.getUserName());
						preparedStatement.setString(2, adsBean.getItemId());
						removedStatus = preparedStatement.executeUpdate();

						if (removedStatus==1) {
							System.out.println("successfully updated the cart");
						} 
					}
					// if there is only 1 same product then delete the
					// item					
						else if ((resultSet.getInt(1) == 1)) {

							preparedStatement = con
									.prepareStatement(removeItem);
							preparedStatement.setString(1,
									adsBean.getUserName());
							preparedStatement.setString(2, adsBean.getItemId());
							removedStatus = preparedStatement.executeUpdate();

							if (removedStatus==1) {
								System.out
										.println("successfully removed the item");
							}
						}
				}else{
					resultsFromCart[0] = 0;
				}
			}
		} catch (Exception exception) {
			System.out.println(exception);
		}
		return resultsFromCart;
	}
}
