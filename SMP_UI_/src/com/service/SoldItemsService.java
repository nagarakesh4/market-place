package com.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.jws.WebService;

import com.beans.AdsBean;
@WebService
public class SoldItemsService {
	public AdsBean[] retrieveSoldItems(String username) {

		Connection con = null;
		AdsBean[] adsBeanValuesSold = new AdsBean[250];
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			con = DriverManager.getConnection("jdbc:mysql://localhost/test",
					"root", "root");
			int index = 0;

			// mysql to retrieve brought and sold items
			String broughtItems = "select * from checkedoutitems where sellerUserName=?";

			PreparedStatement preparedStatementSold = con
					.prepareStatement(broughtItems);
			preparedStatementSold.setString(1, username);
			ResultSet resultSetSold = preparedStatementSold.executeQuery();
			while (resultSetSold.next()) {
				AdsBean adsBean = new AdsBean();
				adsBean.setItemId(resultSetSold.getString(1));
				adsBean.setItemPrice(resultSetSold.getFloat(2));
				adsBean.setItemName(resultSetSold.getString(6));
				adsBean.setQuantity(resultSetSold.getInt(5));
				adsBean.setUserName(resultSetSold.getString(3));
				adsBeanValuesSold[index] = adsBean;
				index++;
			}
		} catch (Exception exception) {
			System.out.println(exception);
			exception.printStackTrace();
		}
		return adsBeanValuesSold;
	}
}
