public class merge {

	public static void main(String[] args) {
		int[] a = { 21, 21, 21 };
		int[] b = { 21, 21, 21 };
		merger m = new merger();
		int[] c = m.merge(a, b);
		for (int i = 0; i < c.length; i++) {
			System.out.println(c[i]);
		}
	}
}

class merger {
	public int[] merge(int[] a, int[] b) {

		int[] answer = new int[a.length + b.length];
		System.out.println(answer.length);
		int i = 0, j = 0, k = 0;
		while (i < a.length && j < b.length) {
			if (a[i] < b[j]) {
				answer[k] = a[i];
				i++;
			} else {
				answer[k] = b[j];
				j++;
			}
			k++;
		}

		while (i < a.length) {
			answer[k] = a[i];
			i++;
			k++;
		}

		while (j < b.length) {
			answer[k] = b[j];
			j++;
			k++;
		}

		return answer;
	}
}