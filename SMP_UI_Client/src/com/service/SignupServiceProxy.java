package com.service;

public class SignupServiceProxy implements com.service.SignupService {
  private String _endpoint = null;
  private com.service.SignupService signupService = null;
  
  public SignupServiceProxy() {
    _initSignupServiceProxy();
  }
  
  public SignupServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initSignupServiceProxy();
  }
  
  private void _initSignupServiceProxy() {
    try {
      signupService = (new com.service.SignupServiceServiceLocator()).getSignupService();
      if (signupService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)signupService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)signupService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (signupService != null)
      ((javax.xml.rpc.Stub)signupService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.service.SignupService getSignupService() {
    if (signupService == null)
      _initSignupServiceProxy();
    return signupService;
  }
  
  public java.lang.String[] signupUser(java.lang.String firstName, java.lang.String lastName, java.lang.String email, java.lang.String password) throws java.rmi.RemoteException{
    if (signupService == null)
      _initSignupServiceProxy();
    return signupService.signupUser(firstName, lastName, email, password);
  }
  
  
}