package com.service;

public class SoldItemsServiceProxy implements com.service.SoldItemsService {
  private String _endpoint = null;
  private com.service.SoldItemsService soldItemsService = null;
  
  public SoldItemsServiceProxy() {
    _initSoldItemsServiceProxy();
  }
  
  public SoldItemsServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initSoldItemsServiceProxy();
  }
  
  private void _initSoldItemsServiceProxy() {
    try {
      soldItemsService = (new com.service.SoldItemsServiceServiceLocator()).getSoldItemsService();
      if (soldItemsService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)soldItemsService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)soldItemsService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (soldItemsService != null)
      ((javax.xml.rpc.Stub)soldItemsService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.service.SoldItemsService getSoldItemsService() {
    if (soldItemsService == null)
      _initSoldItemsServiceProxy();
    return soldItemsService;
  }
  
  public com.beans.AdsBean[] retrieveSoldItems(java.lang.String username) throws java.rmi.RemoteException{
    if (soldItemsService == null)
      _initSoldItemsServiceProxy();
    return soldItemsService.retrieveSoldItems(username);
  }
  
  
}