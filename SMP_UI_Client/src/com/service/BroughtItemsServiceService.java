/**
 * BroughtItemsServiceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.service;

public interface BroughtItemsServiceService extends javax.xml.rpc.Service {
    public java.lang.String getBroughtItemsServiceAddress();

    public com.service.BroughtItemsService getBroughtItemsService() throws javax.xml.rpc.ServiceException;

    public com.service.BroughtItemsService getBroughtItemsService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
