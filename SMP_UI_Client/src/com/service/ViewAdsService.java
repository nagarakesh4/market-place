/**
 * ViewAdsService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.service;

public interface ViewAdsService extends java.rmi.Remote {
    public com.beans.AdsBean[] retrieveAds(java.lang.String username) throws java.rmi.RemoteException;
}
