package com.service;

public class ViewAdsServiceProxy implements com.service.ViewAdsService {
  private String _endpoint = null;
  private com.service.ViewAdsService viewAdsService = null;
  
  public ViewAdsServiceProxy() {
    _initViewAdsServiceProxy();
  }
  
  public ViewAdsServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initViewAdsServiceProxy();
  }
  
  private void _initViewAdsServiceProxy() {
    try {
      viewAdsService = (new com.service.ViewAdsServiceServiceLocator()).getViewAdsService();
      if (viewAdsService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)viewAdsService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)viewAdsService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (viewAdsService != null)
      ((javax.xml.rpc.Stub)viewAdsService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.service.ViewAdsService getViewAdsService() {
    if (viewAdsService == null)
      _initViewAdsServiceProxy();
    return viewAdsService;
  }
  
  public com.beans.AdsBean[] retrieveAds(java.lang.String username) throws java.rmi.RemoteException{
    if (viewAdsService == null)
      _initViewAdsServiceProxy();
    return viewAdsService.retrieveAds(username);
  }
  
  
}