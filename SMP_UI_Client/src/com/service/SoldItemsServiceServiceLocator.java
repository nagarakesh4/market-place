/**
 * SoldItemsServiceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.service;

public class SoldItemsServiceServiceLocator extends org.apache.axis.client.Service implements com.service.SoldItemsServiceService {

    public SoldItemsServiceServiceLocator() {
    }


    public SoldItemsServiceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SoldItemsServiceServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for SoldItemsService
    private java.lang.String SoldItemsService_address = "http://localhost:8080/SMP_UI_/services/SoldItemsService";

    public java.lang.String getSoldItemsServiceAddress() {
        return SoldItemsService_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String SoldItemsServiceWSDDServiceName = "SoldItemsService";

    public java.lang.String getSoldItemsServiceWSDDServiceName() {
        return SoldItemsServiceWSDDServiceName;
    }

    public void setSoldItemsServiceWSDDServiceName(java.lang.String name) {
        SoldItemsServiceWSDDServiceName = name;
    }

    public com.service.SoldItemsService getSoldItemsService() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(SoldItemsService_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getSoldItemsService(endpoint);
    }

    public com.service.SoldItemsService getSoldItemsService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.service.SoldItemsServiceSoapBindingStub _stub = new com.service.SoldItemsServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getSoldItemsServiceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setSoldItemsServiceEndpointAddress(java.lang.String address) {
        SoldItemsService_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.service.SoldItemsService.class.isAssignableFrom(serviceEndpointInterface)) {
                com.service.SoldItemsServiceSoapBindingStub _stub = new com.service.SoldItemsServiceSoapBindingStub(new java.net.URL(SoldItemsService_address), this);
                _stub.setPortName(getSoldItemsServiceWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("SoldItemsService".equals(inputPortName)) {
            return getSoldItemsService();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://service.com", "SoldItemsServiceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://service.com", "SoldItemsService"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("SoldItemsService".equals(portName)) {
            setSoldItemsServiceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
