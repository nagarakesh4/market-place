package com.service;

public class CreateAdsServiceProxy implements com.service.CreateAdsService {
  private String _endpoint = null;
  private com.service.CreateAdsService createAdsService = null;
  
  public CreateAdsServiceProxy() {
    _initCreateAdsServiceProxy();
  }
  
  public CreateAdsServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initCreateAdsServiceProxy();
  }
  
  private void _initCreateAdsServiceProxy() {
    try {
      createAdsService = (new com.service.CreateAdsServiceServiceLocator()).getCreateAdsService();
      if (createAdsService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)createAdsService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)createAdsService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (createAdsService != null)
      ((javax.xml.rpc.Stub)createAdsService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.service.CreateAdsService getCreateAdsService() {
    if (createAdsService == null)
      _initCreateAdsServiceProxy();
    return createAdsService;
  }
  
  public java.lang.String[] createAds(java.lang.String itemName, java.lang.String itemDescription, java.lang.String sellerInformation, java.lang.String itemPrice, java.lang.String quantity, java.lang.String itemId, java.lang.String username) throws java.rmi.RemoteException{
    if (createAdsService == null)
      _initCreateAdsServiceProxy();
    return createAdsService.createAds(itemName, itemDescription, sellerInformation, itemPrice, quantity, itemId, username);
  }
  
  
}