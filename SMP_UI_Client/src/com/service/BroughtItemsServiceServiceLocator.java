/**
 * BroughtItemsServiceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.service;

public class BroughtItemsServiceServiceLocator extends org.apache.axis.client.Service implements com.service.BroughtItemsServiceService {

    public BroughtItemsServiceServiceLocator() {
    }


    public BroughtItemsServiceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public BroughtItemsServiceServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for BroughtItemsService
    private java.lang.String BroughtItemsService_address = "http://localhost:8080/SMP_UI_/services/BroughtItemsService";

    public java.lang.String getBroughtItemsServiceAddress() {
        return BroughtItemsService_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String BroughtItemsServiceWSDDServiceName = "BroughtItemsService";

    public java.lang.String getBroughtItemsServiceWSDDServiceName() {
        return BroughtItemsServiceWSDDServiceName;
    }

    public void setBroughtItemsServiceWSDDServiceName(java.lang.String name) {
        BroughtItemsServiceWSDDServiceName = name;
    }

    public com.service.BroughtItemsService getBroughtItemsService() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(BroughtItemsService_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBroughtItemsService(endpoint);
    }

    public com.service.BroughtItemsService getBroughtItemsService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.service.BroughtItemsServiceSoapBindingStub _stub = new com.service.BroughtItemsServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getBroughtItemsServiceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBroughtItemsServiceEndpointAddress(java.lang.String address) {
        BroughtItemsService_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.service.BroughtItemsService.class.isAssignableFrom(serviceEndpointInterface)) {
                com.service.BroughtItemsServiceSoapBindingStub _stub = new com.service.BroughtItemsServiceSoapBindingStub(new java.net.URL(BroughtItemsService_address), this);
                _stub.setPortName(getBroughtItemsServiceWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("BroughtItemsService".equals(inputPortName)) {
            return getBroughtItemsService();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://service.com", "BroughtItemsServiceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://service.com", "BroughtItemsService"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("BroughtItemsService".equals(portName)) {
            setBroughtItemsServiceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
