package com.service;

public class BroughtItemsServiceProxy implements com.service.BroughtItemsService {
  private String _endpoint = null;
  private com.service.BroughtItemsService broughtItemsService = null;
  
  public BroughtItemsServiceProxy() {
    _initBroughtItemsServiceProxy();
  }
  
  public BroughtItemsServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initBroughtItemsServiceProxy();
  }
  
  private void _initBroughtItemsServiceProxy() {
    try {
      broughtItemsService = (new com.service.BroughtItemsServiceServiceLocator()).getBroughtItemsService();
      if (broughtItemsService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)broughtItemsService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)broughtItemsService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (broughtItemsService != null)
      ((javax.xml.rpc.Stub)broughtItemsService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.service.BroughtItemsService getBroughtItemsService() {
    if (broughtItemsService == null)
      _initBroughtItemsServiceProxy();
    return broughtItemsService;
  }
  
  public com.beans.AdsBean[] retrieveBroughtItems(java.lang.String username) throws java.rmi.RemoteException{
    if (broughtItemsService == null)
      _initBroughtItemsServiceProxy();
    return broughtItemsService.retrieveBroughtItems(username);
  }
  
  
}