/**
 * ViewAdsServiceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.service;

public class ViewAdsServiceServiceLocator extends org.apache.axis.client.Service implements com.service.ViewAdsServiceService {

    public ViewAdsServiceServiceLocator() {
    }


    public ViewAdsServiceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ViewAdsServiceServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ViewAdsService
    private java.lang.String ViewAdsService_address = "http://localhost:8080/SMP_UI_/services/ViewAdsService";

    public java.lang.String getViewAdsServiceAddress() {
        return ViewAdsService_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ViewAdsServiceWSDDServiceName = "ViewAdsService";

    public java.lang.String getViewAdsServiceWSDDServiceName() {
        return ViewAdsServiceWSDDServiceName;
    }

    public void setViewAdsServiceWSDDServiceName(java.lang.String name) {
        ViewAdsServiceWSDDServiceName = name;
    }

    public com.service.ViewAdsService getViewAdsService() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ViewAdsService_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getViewAdsService(endpoint);
    }

    public com.service.ViewAdsService getViewAdsService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.service.ViewAdsServiceSoapBindingStub _stub = new com.service.ViewAdsServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getViewAdsServiceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setViewAdsServiceEndpointAddress(java.lang.String address) {
        ViewAdsService_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.service.ViewAdsService.class.isAssignableFrom(serviceEndpointInterface)) {
                com.service.ViewAdsServiceSoapBindingStub _stub = new com.service.ViewAdsServiceSoapBindingStub(new java.net.URL(ViewAdsService_address), this);
                _stub.setPortName(getViewAdsServiceWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ViewAdsService".equals(inputPortName)) {
            return getViewAdsService();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://service.com", "ViewAdsServiceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://service.com", "ViewAdsService"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ViewAdsService".equals(portName)) {
            setViewAdsServiceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
