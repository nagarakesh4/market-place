package com.Controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.beans.AdsBean;
import com.service.ViewCartServiceProxy;

/**
 * Servlet implementation class DisplayCartController
 */
public class DisplayCartController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayCartController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);
		
		String buname = request.getParameter("buyerUserName");
		System.out.println("this buyer is seeing his cart " +buname);
		
		ViewCartServiceProxy viewCartServiceProxy = new ViewCartServiceProxy();
		viewCartServiceProxy.setEndpoint("http://localhost:8080/SMP_UI_/services/ViewCartService");
		
		
		
		AdsBean[] results = viewCartServiceProxy.retrieveCart(buname);
		int index = 0;
		while (results[index] != null) {
			index++;
		}
		float totalValue = results[index-1].getTotalValueOfItems();
		int totalProductsInCart = results[index-1].getTotalItemsInCart();
		String nextJSP = "viewCart.jsp";
		
		
		session.setAttribute("totalValueOfItems", totalValue);
		session.setAttribute("totalProductsInCart", totalProductsInCart);
		session.setAttribute("totalProducts", index-1);
		session.setAttribute("beanValues", results);
		response.sendRedirect(nextJSP);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
