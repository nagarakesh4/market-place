package com.Controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.beans.AdsBean;
import com.service.BroughtItemsServiceProxy;
import com.service.SoldItemsServiceProxy;

/**
 * Servlet implementation class TransactionsController
 */
public class TransactionsController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TransactionsController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		
		String username = request.getParameter("username");
		System.out.println("In Controller "+username);
		
		BroughtItemsServiceProxy broughtItemsServiceProxy = new BroughtItemsServiceProxy();
		AdsBean[] resultsBrought = broughtItemsServiceProxy.retrieveBroughtItems(username);
		int indexBrought = 0;
		
		SoldItemsServiceProxy soldItemsServiceProxy = new SoldItemsServiceProxy();
		AdsBean[] resultsSold = soldItemsServiceProxy.retrieveSoldItems(username);
		int indexSold = 0;
		
		String nextJSP = "transactions.jsp";
		while (resultsBrought[indexBrought] != null) {
			indexBrought++;
		}
		
		while (resultsSold[indexSold] != null) {
			indexSold++;
		}
		
		
		session.setAttribute("totalSoldItems", indexSold);
		session.setAttribute("totalBroughtItems", indexBrought);
		session.setAttribute("soldItems", resultsSold);
		session.setAttribute("broughtItems", resultsBrought);
		response.sendRedirect(nextJSP);
	}

}
