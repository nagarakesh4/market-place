package com.Controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.beans.AdsBean;
import com.service.ViewAdsServiceProxy;
import com.service.ViewCartServiceProxy;

/**
 * Servlet implementation class ViewAdsController
 */
public class ViewAdsController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ViewAdsController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		System.out.println("In View ads Controller");
		HttpSession session = request.getSession(true);
		String username = request.getParameter("username");

		ViewAdsServiceProxy viewAdsServiceProxy = new ViewAdsServiceProxy();
		viewAdsServiceProxy
				.setEndpoint("http://localhost:8080/SMP_UI_/services/ViewAdsService");
		
			
		AdsBean[] results = viewAdsServiceProxy.retrieveAds(username);
		
		ViewCartServiceProxy viewCartServiceProxy = new ViewCartServiceProxy();
		viewCartServiceProxy.setEndpoint("http://localhost:8080/SMP_UI_/services/ViewCartService");
		AdsBean[] resultsInCart = viewCartServiceProxy.retrieveCart(username);
		int index = 0,cartIndex = 0;
		
		while (results[index] != null) {
			index++;
		}
		
		while (resultsInCart[cartIndex] != null) {
			cartIndex++;
		}
		
		int totalProductsInCart = resultsInCart[cartIndex-1].getTotalItemsInCart();
		System.out.println("in controller total cart "+totalProductsInCart);
		System.out.println("total ads (in controller)" + index);
		String nextJSP = "viewAdvertisements.jsp";
		session.setAttribute("totalProductsInCart", totalProductsInCart);
		session.setAttribute("totalProducts", index);
		session.setAttribute("beanValues", results);
		response.sendRedirect(nextJSP);

	}
}
