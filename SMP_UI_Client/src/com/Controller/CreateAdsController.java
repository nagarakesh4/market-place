package com.Controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.service.CreateAdsServiceProxy;

/**
 * Servlet implementation class CreateAdsController
 */
public class CreateAdsController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreateAdsController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String itemName = request.getParameter("itemName");
		String itemDescription = request.getParameter("itemDescription");
		String sellerInformation = request.getParameter("sellerInformation");
		String itemPrice = request.getParameter("itemPrice");
		String quantity = request.getParameter("quantity");
		String itemId = request.getParameter("itemId");
		String userName = request.getParameter("username");
		CreateAdsServiceProxy createAdsServiceProxy = new CreateAdsServiceProxy();
		createAdsServiceProxy
				.setEndpoint("http://localhost:8080/SMP_UI_/services/CreateAdsService");
		String[] adCreate = createAdsServiceProxy.createAds(itemName,
				itemDescription, sellerInformation, itemPrice,
				quantity, itemId, userName);
		if (adCreate[1].equals("1")) {
			String nextJSP = "/homePage.jsp";
			redirectToNextPage(nextJSP, itemId, request, response,
					adCreate);
		} else {
			String nextJSP = "/createAdvertisement.jsp";
			redirectToNextPage(nextJSP, itemId, request, response,
					adCreate);
		}
	}

	private void redirectToNextPage(String nextJSP, String itemId,
			HttpServletRequest request, HttpServletResponse response,
			String[] adCreate) throws ServletException, IOException {
		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher(nextJSP);
		request.setAttribute("itemId", itemId);
		request.setAttribute("createAdStatus", adCreate[1]);
		request.setAttribute("createAdMessage", adCreate[2]);
		dispatcher.forward(request, response);
	}
}