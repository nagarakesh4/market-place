package com.Controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.service.SignupServiceProxy;

/**
 * Servlet implementation class SignupController
 */
public class SignupController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SignupController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
		String lastName = request.getParameter("lastName");
		String firstName = request.getParameter("firstName");
		String emailid = request.getParameter("emailid");
		String password = request.getParameter("password");

		SignupServiceProxy signupServiceProxy = new SignupServiceProxy();
		signupServiceProxy
				.setEndpoint("http://localhost:8080/SMP_UI_/services/SignupService");
		String[] signupStatus = signupServiceProxy.signupUser(firstName,
				lastName, emailid, password);

		System.out.println("in servlet "+signupStatus[0]);
		System.out.println("in servlet message "+signupStatus[1]);
		if (signupStatus[1].equals("1")) {
			String nextJSP = "/loginPage.jsp";
			redirectToNextPage(nextJSP,emailid, request, response, signupStatus);
		} else {
			String nextJSP = "/signUp.jsp";
			redirectToNextPage(nextJSP,emailid, request, response, signupStatus);
		}
	}

	private void redirectToNextPage(String nextJSP, String emailid,
			HttpServletRequest request, HttpServletResponse response,
			String[] signupStatus) throws ServletException, IOException {
		
		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher(nextJSP);
		request.setAttribute("username", emailid);
		request.setAttribute("signupStatus", signupStatus[1]);
		request.setAttribute("signupMessage", signupStatus[2]);
		dispatcher.forward(request, response);
	}
}
