package com.Controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.service.PaymentServiceProxy;

/**
 * Servlet implementation class PaymentController
 */
public class PaymentController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PaymentController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String ccnumber = request.getParameter("ccNumber");
		String buyerUserName = request.getParameter("buyerUserName");
		HttpSession session = request.getSession(true);
		int paymentStatus = 0;
		PaymentServiceProxy paymentServiceProxy = new PaymentServiceProxy();
		paymentServiceProxy.setEndpoint("http://localhost:8080/SMP_UI_/services/PaymentService");
		paymentStatus = paymentServiceProxy.checkOut(buyerUserName,ccnumber);
		String nextJSP = "/checkedOut.jsp";
		if(paymentStatus==1){
			nextJSP = "/finalPage.jsp";
		}
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
		request.setAttribute("paymentStatus", paymentStatus);
		session.setAttribute("totalProductsInCart", 0);
		dispatcher.forward(request,response);
	}

}
